//
//  HomeController.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

public protocol HomeControllerDelegate {
    func handleMenuToggle()
}

public class HomeController: UIViewController {

    public var delegate: HomeControllerDelegate?
    public var tap = UITapGestureRecognizer()

    public var isExpanded: Bool?

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .blue
        self.configureNavigationBar()

        self.tap = UITapGestureRecognizer(target: self, action: #selector(handleMenuToggle))
    }

    public func configureNavigationBar() {
        self.navigationItem.title = "LeftMenuToIG"

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(handleMenuToggle))
    }

    @objc public func handleMenuToggle() {
        delegate?.handleMenuToggle()
        if !(self.isExpanded ?? false) {
            self.setGesture()
            self.isExpanded = true
        } else {
            self.removeGesture()
            self.isExpanded = false
        }
    }

    public func setGesture() {
        self.view.addGestureRecognizer(tap)
    }

    public func removeGesture() {
        self.view.removeGestureRecognizer(tap)
    }
}
