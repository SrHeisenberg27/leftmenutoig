//
//  DrawerProtocol.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import Foundation

public protocol DrawerProtocol {
    var cellDrawer: CellDrawerProtocol { get }
}
