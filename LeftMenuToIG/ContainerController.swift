//
//  ContainerController.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

public class ContainerController: UIViewController {

    var menuController: MenuController!
    var centerController: UIViewController!
    
    public var isExpanded = false

    public override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeController()
    }

    public func configureHomeController() {
        let homeController = HomeController()
        homeController.delegate = self
        homeController.isExpanded = self.isExpanded
        centerController = UINavigationController(rootViewController: homeController)

        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }

    public func configureMenuController() {
        if menuController == nil {
            menuController = MenuController()
            menuController.delegate = self
            menuController.container = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
    }

    public func animatePanel(shouldExpand: Bool) {
        if shouldExpand {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            })
        }

        self.animateStatusBar()
    }

    public func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
}

extension ContainerController: HomeControllerDelegate {
    public func handleMenuToggle() {
        if !isExpanded {
            configureMenuController()
        }

        isExpanded = !isExpanded
        animatePanel(shouldExpand: isExpanded)

    }
}
