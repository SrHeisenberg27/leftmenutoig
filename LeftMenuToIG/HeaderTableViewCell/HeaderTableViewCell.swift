//
//  HeaderTableViewCell.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

public class HeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
}
