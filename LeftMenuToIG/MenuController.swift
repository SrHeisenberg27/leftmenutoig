//
//  MenuController.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class MenuController: UIViewController {

    var container: ContainerController?
    
    var tableView: UITableView?
    var delegate: HomeControllerDelegate?

    var cells = [DrawerProtocol]()

    var titleCell1: String = "Titulo 1"
    var titleCell2: String = "Titulo 2"
    var titleCell3: String = "Titulo 3"

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }

    func configureTableView() {
        tableView = UITableView()
        tableView?.delegate = self
        tableView?.dataSource = self

        tableView?.register(UINib.init(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCellIID")
        tableView?.register(UINib.init(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCellID")

        tableView?.backgroundColor = .lightGray
        tableView?.separatorStyle = .none
        tableView?.rowHeight = 80

        view.addSubview(tableView ?? UITableView())
        tableView?.translatesAutoresizingMaskIntoConstraints = false
        tableView?.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView?.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView?.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView?.topAnchor.constraint(equalTo: view.topAnchor).isActive = true

        cells.append(HeaderTableViewModel(labelText: "Cabecera", iconCell: UIImage(named: "headerIcon") ?? UIImage()))

        cells.append(MenuTableViewModel(labelText: self.titleCell1, iconCell: UIImage(named: "icon1") ?? UIImage()))
        cells.append(MenuTableViewModel(labelText: self.titleCell2, iconCell: UIImage(named: "icon2") ?? UIImage()))
        cells.append(MenuTableViewModel(labelText: self.titleCell3, iconCell: UIImage(named: "icon3") ?? UIImage()))
    }
}

extension MenuController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = cells[indexPath.row]
        let drawer = item.cellDrawer
        let cell = drawer.tableView(tableView, cellForRowAt: indexPath)
        drawer.drawCell(cell, withItem: item)
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = cells[indexPath.row] as? HeaderTableViewModel {
            print("cabecera")
        } else if let cell = cells[indexPath.row] as? MenuTableViewModel {
            switch cell.labelText {
            case titleCell1:
                print("titleCell1")
            case titleCell2:
                print("titleCell2")
            case titleCell3:
                print("titleCell3")
            default:
                break
            }

            container?.handleMenuToggle()

            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            if let exampleViewController = storyBoard.instantiateViewController(withIdentifier: "ExampleVC") as? ExampleVC {
                exampleViewController.labelFromMenu = cell.labelText
                exampleViewController.modalPresentationStyle = .fullScreen

                self.presentDetail(exampleViewController)
            }
        }
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
}

extension UIViewController {

    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)

        present(viewControllerToPresent, animated: false)
    }

    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)

        dismiss(animated: false)
    }
}
