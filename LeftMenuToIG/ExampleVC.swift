//
//  ExampleVC.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class ExampleVC: UIViewController {

    @IBOutlet weak var buttonExample: UIButton!

    var labelFromMenu: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.buttonExample.setTitle(self.labelFromMenu, for: .normal)

        self.buttonExample.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
    }

    @objc func backAction(_ sender: UIButton) {
        dismissDetail()
    }
}
