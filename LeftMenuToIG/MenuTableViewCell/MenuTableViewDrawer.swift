//
//  MenuTableViewDrawer.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

public final class MenuTableViewDrawer: CellDrawerProtocol {
    private struct Constants {
        static let reuseID = "MenuTableViewCellID"
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: Constants.reuseID, for: indexPath)
    }

    public func drawCell(_ cell: UITableViewCell, withItem item: Any) {
        guard let cell = cell as? MenuTableViewCell,
            let item = item as? MenuTableViewModel else { return }

        cell.selectionStyle = .none

        cell.labelText.text = item.labelText
        cell.iconImage.image = item.iconCell
    }
}

extension MenuTableViewModel: DrawerProtocol {
    public var cellDrawer: CellDrawerProtocol {
        return MenuTableViewDrawer()
    }
}
