//
//  MenuTableViewModel.swift
//  LeftMenuToIG
//
//  Created by Álvaro Ferrández Gómez on 12/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

public struct MenuTableViewModel {
    var labelText: String
    var iconCell: UIImage
}
